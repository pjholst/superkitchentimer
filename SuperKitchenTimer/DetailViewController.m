//
//  DetailViewController.m
//  SuperKitchenTimer
//
//  Created by Holst, Phil on 8/9/14.
//  Copyright (c) 2014 HolstSoft. All rights reserved.
//

#import "DetailViewController.h"

#import "Timer.h"
#import "TimerView.h"

#define kComponentHour 0
#define kComponentMinute 1
#define kComponentSecond 2

@interface DetailViewController ()

@property (strong, nonatomic) UIPopoverController *masterPopoverController;

- (void)setupTimerView;

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setTimer:(Timer *)timer
{
    if (_timer != timer)
	{
        _timer = timer;
    }

    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }        
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if (self.timer) {
	    self.detailDescriptionLabel.text = [self.timer description];
        [_stylePicker removeAllSegments];
        [_stylePicker insertSegmentWithTitle:@"Picker" atIndex:0 animated:NO];
        [_stylePicker insertSegmentWithTitle:@"Clock" atIndex:1 animated:NO];
        
        [_stylePicker setSelectedSegmentIndex:0];
	}
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [(TimerManager*)[TimerManager sharedManager] setDelegate:self];
	_titleText.text = _timer._timerName;
	NSTimeInterval interval = _timer._active == YES ? [_timer timeLeftForTimer] : _timer._setInterval;
    [self setupTimerView];
    
    // Setup the Picker
    [_timerView setTimerInterval:interval];
    [self setupStartStopButton];
}

- (void)viewWillDisappear:(BOOL)animated
{
	_timer._setInterval = [_timerView getTimerInterval];
    [_timerView removeFromSuperview];
    
	[super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)timerButton:(id)sender
{
    if ( _timer._active == NO )
    {
        NSTimeInterval tInt = [_timerView getTimerInterval];
        if ( tInt >= 10 )
            [_timer startTimerWithInterval:tInt andNameString:_titleText.text];
        else
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Timer Error"
                                                            message:@"Please start timer with at least 10 seconds"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
    else
    {
        [_timer stopTimer];
    }
    [self setupStartStopButton];
}

- (IBAction)StyleSegmentSelected:(id)sender
{
    if (!_timer._active)
    {
        _timer._setInterval = [_timerView getTimerInterval];
    }
    [_timerView setTimerStyle:(enum TimerStyles)_stylePicker.selectedSegmentIndex];
    [_timerView setTimerInterval:[_timer timeLeftForTimer]];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	if ( textField == _titleText )
	{
        [_timer updateName:_titleText.text];
	}
	
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	return YES;
}

- (void)TimerUpdated;
{
	dispatch_async( dispatch_get_main_queue(), ^{
		if ( self.view.window.isKeyWindow && _timer._active )
		{
            [_timerView setTimerInterval:[_timer timeLeftForTimer]];
			//NSLog( @"Timer Fired" );
		}
	} );
}

- (void)setupStartStopButton
{
    if ( _timer._active )
    {
        [_timerButton setTitle:NSLocalizedString(@"StopButton", nil) forState:UIControlStateNormal];
        [_timerButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    }
    else
    {
        [_timerButton setTitle:NSLocalizedString(@"StartButton", nil) forState:UIControlStateNormal];
        [_timerButton setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
    }
}

- (void)setupTimerView
{
    if (!_timerView)
    {
        _timerView = [[TimerView alloc] initWithFrame:CGRectMake(20, 123, 280, 162)];
    }
    [self.view addSubview:_timerView];
}

#pragma mark - Rotation Methods

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [_timerView removeFromSuperview];
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self setupTimerView];
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Master", @"Master");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}

@end
