//
//  MasterViewController.h
//  SuperKitchenTimer
//
//  Created by Holst, Phil on 8/9/14.
//  Copyright (c) 2014 HolstSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimerManager.h"

@class DetailViewController;

@interface MasterViewController : UITableViewController <TimerUpdateDelegate>

@property (strong, nonatomic) DetailViewController *detailViewController;

@end
