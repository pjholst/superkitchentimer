//
//  TimerManager.h
//  SuperKitchenTimer
//
//  Created by Holst, Phil on 8/9/14.
//  Copyright (c) 2014 HolstSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Timer;

@protocol TimerUpdateDelegate

@required
- (void)TimerUpdated;

@end

@interface TimerManager : NSObject

@property (weak) id<TimerUpdateDelegate> delegate;

+ (id)sharedManager;

- (id)addTimer:(NSInteger)atIndex;
- (void)removeTimer:(NSInteger)atIndex;
- (NSInteger)getCount;
- (NSString*)getDescription:(NSInteger)forIndex;
- (id)getTimer:(NSInteger)atIndex;
- (NSInteger)getIndex:(Timer*)forTimer;

#pragma mark TimerThreadStuff
- (void)timerFired;
- (void)timerCancelled;

@end
