//
//  ClockView.m
//  SuperKitchenTimer
//
//  Created by Philip Holst on 9/20/14.
//  Copyright (c) 2014 HolstSoft. All rights reserved.
//

#import "ClockView.h"

@implementation ClockView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat smallerSide = frame.size.width < frame.size.height ? frame.size.width : frame.size.height;
        CGFloat largerSide = frame.size.width < frame.size.height ? frame.size.height : frame.size.width;
        CGFloat extra = largerSide - smallerSide;
        
        
        CGRect clockRect = CGRectMake( extra / 2, 0, smallerSide, smallerSide);
        
        _clockView = [[UIImageView alloc] initWithFrame:clockRect];
        _hourView = [[UIImageView alloc] initWithFrame:clockRect];
        _minuteView = [[UIImageView alloc] initWithFrame:clockRect];
        _secondView = [[UIImageView alloc] initWithFrame:clockRect];
        
        [_clockView setImage:[UIImage imageNamed:@"ClockFace.png"]];
        [_hourView setImage:[UIImage imageNamed:@"HourHand.png"]];
        [_minuteView setImage:[UIImage imageNamed:@"MinuteHand.png"]];
        [_secondView setImage:[UIImage imageNamed:@"SecondHand.png"]];
        
        [self addSubview:_clockView];
        [self addSubview:_hourView];
        [self addSubview:_minuteView];
        [self addSubview:_secondView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark - Animation of Hands
#define degreesToRadians(deg) (deg / 180.0 * M_PI)

- (void)moveHourToPosition:(NSInteger)hour withMinute:(NSInteger)minute
{
    int degreesPerHour   = 360.f / 12.f; // 30
    float degreesPerMinute = degreesPerHour / 60.f;
    
    float rotationForHoursComponent  = hour * degreesPerHour;
    float rotationForMinuteComponent = degreesPerMinute * minute;
    
    float totalRotation = rotationForHoursComponent + rotationForMinuteComponent;

   double hourAngle = degreesToRadians(totalRotation);
    
    [UIView animateWithDuration:0.2 animations:^(void){
        _hourView.transform = CGAffineTransformMakeRotation(hourAngle);
    }];
}

- (void)moveMinuteToPosition:(NSInteger)minute withSecond:(NSInteger)second
{
    int degreesPerMin = 360.f / 60.f; // 6
    float degreesPerSecond = 6.f / 60.f;
    
    float rotationForMinuteComponent  = minute * degreesPerMin;
    float rotationForSecondComponent = degreesPerSecond * second;
    
    float totalRotation = rotationForMinuteComponent + rotationForSecondComponent;
    
    double minAngle = degreesToRadians(totalRotation);
    
    [UIView animateWithDuration:0.2 animations:^(void){
        _minuteView.transform = CGAffineTransformMakeRotation(minAngle);
    }];
}

- (void)moveSecondToPosition:(NSInteger)second
{
    int degreesPerSec = 360.f / 60.f; // 6
    
    float rotationForHoursComponent  = second * degreesPerSec;
    
    float totalRotation = rotationForHoursComponent;
    double secondAngle = degreesToRadians(totalRotation);
    
    [UIView animateWithDuration:0.2 animations:^(void){
        _secondView.transform = CGAffineTransformMakeRotation(secondAngle);
    }];
}

#pragma mark - TimerViewDelegate Methods

- (NSTimeInterval)getTimerInterval
{
    return lastSetInterval;
}

- (void)setTimerInterval:(NSTimeInterval)interval
{
    lastSetInterval = interval;
    
    interval = ABS( interval );
	unsigned long seconds = interval;
	unsigned long minutes = seconds / 60;
	seconds %= 60;
	unsigned long hours = minutes / 60;
	minutes %= 60;
    
    [self moveHourToPosition:hours withMinute:minutes];
    [self moveMinuteToPosition:minutes withSecond:seconds];
    [self moveSecondToPosition:seconds];
}

@end
