//
//  LabeledPickerView.h
//  SuperKitchenTimer
//
//  Created by Philip Holst on 9/6/14.
//  Copyright (c) 2014 HolstSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimerView.h"

@interface LabeledPickerView : UIPickerView <TimerViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
{
    NSMutableDictionary *labels;
}

@property (strong, nonatomic) UIColor* textColor;
@property (strong, nonatomic) NSArray* pickerSource;

/** Adds the label for the given component. */
-(void)addLabel:(NSString *)labeltext forComponent:(NSUInteger)component;
-(void)removeLabels;
@end
