//
//  Timer.h
//  Super Kitchen Timer
//
//  Created by Holst, Phil on 8/9/14.
//  Copyright (c) 2014 HolstSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Timer : NSObject

@property (strong, nonatomic) NSString * _timerName;
@property ( nonatomic) BOOL _active;
@property ( nonatomic ) BOOL _expired;
@property ( nonatomic ) NSTimeInterval _setInterval;
@property (strong, nonatomic) UILocalNotification* _notification;

- (void)startTimerWithInterval:(NSTimeInterval)intervalValue andNameString:(NSString*)nameString;
- (void)stopTimer;
- (NSTimeInterval)timeLeftForTimer;
- (NSString *)formatInterval:(NSTimeInterval)interval includeHour:(BOOL)hourIncluded;
- (NSString*)getDescription;
- (void)updateName:(NSString*)nameString;

@end
