//
//  DetailViewController.h
//  SuperKitchenTimer
//
//  Created by Holst, Phil on 8/9/14.
//  Copyright (c) 2014 HolstSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimerManager.h"

@class TimerView;
@class Timer;

@interface DetailViewController : UIViewController <UISplitViewControllerDelegate, UITextFieldDelegate, TimerUpdateDelegate>

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@property (strong, nonatomic) Timer *timer;
@property (strong, nonatomic) TimerView *timerView;
@property (strong, nonatomic) UIColor *textColor;
@property (weak, nonatomic) IBOutlet UIButton *timerButton;
@property (weak, nonatomic) IBOutlet UISegmentedControl* stylePicker;
@property (weak) IBOutlet UITextField *titleText;

- (IBAction)StyleSegmentSelected:(id)sender;
- (void)setupStartStopButton;

@end
