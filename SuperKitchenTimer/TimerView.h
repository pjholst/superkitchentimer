//
//  GeneralTimerView.h
//  SuperKitchenTimer
//
//  Created by Philip Holst on 9/14/14.
//  Copyright (c) 2014 HolstSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

enum TimerStyles
{
    PickerStyle,
    ClockStyle
};


@protocol TimerViewDelegate <NSObject>

@required
- (NSTimeInterval)getTimerInterval;
- (void)setTimerInterval:(NSTimeInterval)interval;

@end

@interface TimerView : UIView

@property (strong, atomic) id<TimerViewDelegate> timerDelegate;

- (NSTimeInterval)getTimerInterval;
- (void)setTimerInterval:(NSTimeInterval)interval;
- (void)setTimerStyle:(enum TimerStyles)style;

@end
