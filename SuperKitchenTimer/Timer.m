//
//  Timer.m
//  Super Kitchen Timer
//
//  Created by Holst, Phil on 8/9/14.
//  Copyright (c) 2014 HolstSoft. All rights reserved.
//

#import "Timer.h"

@interface Timer ()
{

}

@end

@implementation Timer

@synthesize _timerName, _active, _expired, _setInterval, _notification;

- (id)init
{
    self = [super init];
	if ( self )
	{
		_timerName = NSLocalizedString(@"Untitled", nil);
        _notification = [[UILocalNotification alloc] init];
	}
	return self;
}

- (void)dealloc
{
    [[UIApplication sharedApplication] cancelLocalNotification:_notification];
}

- (void)startTimerWithInterval:(NSTimeInterval)intervalValue andNameString:(NSString*)nameString
{
    [self updateName:nameString];
    
    _notification.fireDate = [[NSDate alloc] initWithTimeIntervalSinceNow:intervalValue];
    _notification.alertBody = [NSString stringWithFormat:NSLocalizedString(@"Timer is done: %@", nil), _timerName];
    _notification.soundName = UILocalNotificationDefaultSoundName;
    _notification.alertAction = @"View Details";
	_active = YES;
	_expired = NO;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:_notification];
}

- (void)stopTimer
{
    [[UIApplication sharedApplication] cancelLocalNotification:_notification];
    _active = NO;
}

- (NSTimeInterval)timeLeftForTimer
{
    return _active ? [_notification.fireDate timeIntervalSinceNow] : _setInterval;
}

- (NSString *)formatInterval:(NSTimeInterval)interval includeHour:(BOOL)hourIncluded
{
	unsigned long seconds = interval;
	unsigned long minutes = seconds / 60;
	seconds %= 60;
	unsigned long hours = minutes / 60;
	minutes %= 60;
	
    NSString* result;
    
	if(hourIncluded || hours > 0)
        result = [NSString stringWithFormat:@"%02ld:%02ld:%02ld", hours, minutes, seconds];
    else
        result = [NSString stringWithFormat:@"%02ld:%02ld", minutes, seconds];
    
	return result;
}

- (NSString*)getDescription
{
	NSInteger time = _active == YES ? ABS((NSInteger)[self timeLeftForTimer]) : ABS(_setInterval);
    NSString* formattedTimeStr = [self formatInterval:time includeHour:YES];
    
	NSString* activeStr = _active == YES ? NSLocalizedString(@"Running", nil) : NSLocalizedString(@"Paused", nil);
	NSString* negStr = _expired == YES ? @"-" : @"";
    return [NSString stringWithFormat:@"%@ %@%@ %@", _timerName, negStr, formattedTimeStr, activeStr];
}

- (void)updateName:(NSString *)nameString
{
    _timerName = nameString;
    _notification.alertBody = [NSString stringWithFormat:NSLocalizedString(@"Timer is done: %@", nil), nameString];
}

@end
