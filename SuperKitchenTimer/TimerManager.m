//
//  TimerManager.m
//  SuperKitchenTimer
//
//  Created by Holst, Phil on 8/9/14.
//  Copyright (c) 2014 HolstSoft. All rights reserved.
//

#import "TimerManager.h"

#import "Timer.h"

@interface TimerManager ()
{
	NSMutableArray *_timers;
	dispatch_source_t	_viewTimer;
	dispatch_queue_t	_viewQueue;
}

@end

@implementation TimerManager

+ (id)sharedManager
{
	static TimerManager* manager = nil;
	static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
    });
	
	return manager;
}

- (id)init
{
	self = [super init];
	if ( self )
	{
		_viewQueue = dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0 );
		_viewTimer = dispatch_source_create( DISPATCH_SOURCE_TYPE_TIMER, 0, 0, _viewQueue );
		dispatch_source_set_timer( _viewTimer, dispatch_time( DISPATCH_TIME_NOW, 0 ), 1 * NSEC_PER_SEC, 0.5 * NSEC_PER_SEC );
		dispatch_source_set_event_handler( _viewTimer, ^{ [self timerFired]; } );
		dispatch_source_set_cancel_handler( _viewTimer, ^{ [self timerCancelled]; } );
		dispatch_resume( _viewTimer );
	}
	
	return self;
}

- (void)dealloc
{
	dispatch_suspend( _viewTimer );
}

-(id)addTimer:(NSInteger)atIndex
{
	if ( !_timers )
		_timers = [[NSMutableArray alloc] init];
	
	Timer *newTimer = [[Timer alloc] init];
	[_timers insertObject:newTimer atIndex:atIndex];
	
	return newTimer;
}

- (void)removeTimer:(NSInteger)atIndex
{
	[_timers removeObjectAtIndex:atIndex];
}

- (NSInteger)getCount
{
	return _timers.count;
}

- (NSString*)getDescription:(NSInteger)forIndex
{
	return [(Timer*)[_timers objectAtIndex:forIndex] getDescription];
}

// This returns an id instead of a Timer* to enforce the idea that the Timer
// can be passed annoymously through classes (usually view controllers)
// without that class knowing what it is.
- (id)getTimer:(NSInteger)atIndex
{
	return [_timers objectAtIndex:atIndex];
}

- (NSInteger)getIndex:(id)forTimer
{
	return [_timers indexOfObject:forTimer];
}

#pragma mark TimerThreadStuff
- (void)timerFired
{
	for ( Timer* timer in _timers )
	{
		if ( timer._active == YES && timer._expired == NO && [timer timeLeftForTimer] <= 0 )
		{
			timer._expired = YES;
		}
	}
	if ( self.delegate )
	{
		[self.delegate TimerUpdated];
	}
}

- (void)timerCancelled
{
	
}

@end
