//
//  ClockView.h
//  SuperKitchenTimer
//
//  Created by Philip Holst on 9/20/14.
//  Copyright (c) 2014 HolstSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimerView.h"

@interface ClockView : UIView <TimerViewDelegate>
{
    @private
    NSTimeInterval lastSetInterval; // Just to make a lot of things easier later.
}

@property (nonatomic, strong) UIImageView* clockView;
@property (nonatomic, strong) UIImageView* hourView;
@property (nonatomic, strong) UIImageView* minuteView;
@property (nonatomic, strong) UIImageView* secondView;

- (void)moveHourToPosition:(NSInteger)hour withMinute:(NSInteger)minute;
- (void)moveMinuteToPosition:(NSInteger)minute withSecond:(NSInteger)second;
- (void)moveSecondToPosition:(NSInteger)second;

@end
