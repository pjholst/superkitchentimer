//
//  LabeledPickerView.m
//  SuperKitchenTimer
//
//  Created by Philip Holst on 9/6/14.
//  Copyright (c) 2014 HolstSoft. All rights reserved.
//

#import "LabeledPickerView.h"

#define kComponentHour 0
#define kComponentMinute 1
#define kComponentSecond 2

@implementation LabeledPickerView

/** loading programmatically */
- (id)initWithFrame:(CGRect)aRect {
    if (self = [super initWithFrame:aRect]) {
        labels = [[NSMutableDictionary alloc] initWithCapacity:3];
        
        self.delegate = self;
        self.dataSource = self;
        
        [self setupPickerSource];
        
        [self addLabel:NSLocalizedString(@"Hr", nil)
                 forComponent:kComponentHour];
        
        [self addLabel:NSLocalizedString(@"Min", nil)
                 forComponent:kComponentMinute];
        
        [self addLabel:NSLocalizedString(@"Sec", nil)
                 forComponent:kComponentSecond];
    }
    return self;
}

/** loading from nib */
- (id)initWithCoder:(NSCoder *)coder {
    if (self = [super initWithCoder:coder]) {
        labels = [[NSMutableDictionary alloc] initWithCapacity:3];
    }
    return self;
}

#pragma mark Labels

-(void)addLabel:(NSString *)labeltext forComponent:(NSUInteger)component
{
    [labels setObject:labeltext forKey:[NSNumber numberWithUnsignedInteger:component]];
}

-(void)removeLabels
{
    for(int component = 0; component < self.numberOfComponents; component++ )
    {
        UIView* view = [self viewWithTag:component + 1];
        [view removeFromSuperview];
    }
    
    [labels removeAllObjects];
}

/**
 Adds the labels to the view, below the selection indicator glass-thingy.
 The labels are aligned to the right side of the wheel.
 The delegate is responsible for providing enough width for both the value and the label.
 */
- (void)didMoveToWindow
{
    // exit if view is removed from the window or there are no labels.
    if (!self.window || [labels count] == 0)
        return;
    
    UIFont *labelFont = [UIFont systemFontOfSize:12];
    
    // find the width of all the wheels combined
    CGFloat widthOfWheels = 0;
    for (int i=0; i<self.numberOfComponents; i++)
    {
        widthOfWheels += [self rowSizeForComponent:i].width;
    }
    
    // find the left side of the first wheel.
    CGFloat wheelLeftSideOffset = (self.frame.size.width - widthOfWheels) / 2;
    
    for ( int component = 0; component < self.numberOfComponents; component++ )
    {
        NSString* labelText = [labels objectForKey:[NSNumber numberWithUnsignedInteger:component]];
        if ( labelText )
        {
            CGRect frame;
            frame.size = [self rowSizeForComponent:component];
            // center it vertically
            frame.origin.y = (self.frame.size.height / 2) - (frame.size.height / 2) - 0.5;
            frame.origin.x = wheelLeftSideOffset;
            
            UILabel *label = [[UILabel alloc] initWithFrame:frame];
            label.text = labelText;
            label.font = labelFont;
            label.backgroundColor = [UIColor clearColor];
            label.shadowColor = [UIColor whiteColor];
            label.shadowOffset = CGSizeMake(0,1);
            [label setAlpha:0.25f];
            label.tag = component + 1;
            label.textAlignment = NSTextAlignmentLeft;
            
            [self addSubview:label];
            
            // Add the width to find the left size of the next wheel.
            wheelLeftSideOffset += frame.size.width;
        }
    }
}

#pragma mark UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	NSInteger retVal;
	
	if ( component == kComponentHour )
		retVal = MAX_HOURS;
	else
		retVal = MAX_MINUTES;
	
	return retVal;
}

- (void)setupPickerSource
{
	if ( !_pickerSource )
	{
		NSMutableArray *tempArray = [[NSMutableArray alloc] init];
		
		NSArray* hourArray = [self setupNumberArray:MAX_HOURS];
		NSArray* minuteArray = [self setupNumberArray:MAX_MINUTES];
		NSArray* secondArray = [self setupNumberArray:MAX_SECONDS];
		
		[tempArray addObject:hourArray];
		[tempArray addObject:minuteArray];
		[tempArray addObject:secondArray];
		
		_pickerSource = [NSArray arrayWithArray:tempArray];
	}
}

- (NSArray*)setupNumberArray:(int)withTotal
{
	NSMutableArray* tempArray = [[NSMutableArray alloc] init];
	
	for ( int i = 0; i < withTotal; i++ )
	{
		[tempArray addObject:[[NSNumber numberWithInt:i] stringValue]];
	}
	
	return tempArray;
}

#pragma mark - UIPickerViewDelegate

- (UIView*)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 65, 25)];
    label.textColor = _textColor;
    label.font = [UIFont systemFontOfSize:18];
    label.text = _pickerSource[component][row];
    label.textAlignment = NSTextAlignmentCenter;
    
    return label;
}


#pragma mark - TimerViewDelegate

- (NSTimeInterval)getTimerInterval
{
    // Get each time from each component, then convert to seconds.
	NSInteger hour = [self selectedRowInComponent:kComponentHour] * hourMultiplier;
	NSInteger minute = [self selectedRowInComponent:kComponentMinute] * minuteMultipler;
	NSInteger second = [self selectedRowInComponent:kComponentSecond];
	
	NSTimeInterval tInt = hour + minute + second;
	
	return tInt;
}

- (void)setTimerInterval:(NSTimeInterval)interval
{
    if ( interval >= 0 )
		_textColor = [UIColor blackColor];
	else
		_textColor = [UIColor redColor];
	
	interval = ABS( interval );
	unsigned long seconds = interval;
	unsigned long minutes = seconds / 60;
	seconds %= 60;
	unsigned long hours = minutes / 60;
	minutes %= 60;
	
	[self selectRow:MIN( hours, MAX_HOURS - 1 ) inComponent:0 animated:YES];
	[self selectRow:MIN( minutes, MAX_MINUTES - 1 ) inComponent:1 animated:YES];
	[self selectRow:MIN( seconds, MAX_SECONDS - 1 ) inComponent:2 animated:YES];
	
	[self reloadAllComponents];
}

@end
