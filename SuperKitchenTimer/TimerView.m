//
//  TimerView.m
//  SuperKitchenTimer
//
//  Created by Philip Holst on 9/14/14.
//  Copyright (c) 2014 HolstSoft. All rights reserved.
//

#import "TimerView.h"

#import "ClockView.h"
#import "LabeledPickerView.h"

@implementation TimerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setTimerStyle:PickerStyle];
    }
    return self;
}

- (NSTimeInterval)getTimerInterval
{
    NSTimeInterval retVal = 0;
    if ([[_timerDelegate class] conformsToProtocol:@protocol(TimerViewDelegate)]) {
        retVal = [_timerDelegate getTimerInterval];
    }
    return retVal;
}

- (void)setTimerInterval:(NSTimeInterval)interval
{
    if ([[_timerDelegate class] conformsToProtocol:@protocol(TimerViewDelegate)])
    {
        [_timerDelegate setTimerInterval:interval];
    }
}

- (void)setTimerStyle:(enum TimerStyles)style
{
    if (_timerDelegate && [_timerDelegate isKindOfClass:[UIView class]])
    {
        [(UIView*)_timerDelegate removeFromSuperview];
    }
    
    CGRect viewFrame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    id newView = nil;
    
    switch (style) {
        case PickerStyle:
        {
            newView = [[LabeledPickerView alloc] initWithFrame:viewFrame];
            break;
        }
        
        case ClockStyle:
        {
            newView = [[ClockView alloc] initWithFrame:viewFrame];
            break;
        }
            
        default:
            break;
    }
    
    if (newView)
    {
        _timerDelegate = newView;
        [self addSubview:newView];
    }
}

@end
