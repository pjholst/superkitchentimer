//
//  TimerTests.m
//  SuperKitchenTimer
//
//  Created by Holst, Phil on 8/21/14.
//  Copyright (c) 2014 HolstSoft. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <objc/runtime.h>

#import "Timer.h"
#import "NSDate-Extensions.h"


@interface TimerTests : XCTestCase
{
    Timer* timer;
}

@end

@interface TimerTestsWithIntervalSizzle : TimerTests
{
    Method orginalIntervalMethod;
    Method altIntervalMethod;
}

@end

@implementation TimerTests

- (void)setUp
{
    [super setUp];
    timer = [[Timer alloc] init];
    timer._notification.fireDate = [NSDate date];
}

- (void)tearDown
{
    [timer stopTimer]; // Just in case
    [super tearDown];
}

- (void)testStartTimerWithInterval
{
    NSString* testStr = @"Test Name";
    timer._timerName = testStr;
    
    
}

- (void)testUpdateName
{
    NSString* testStr = @"Test Name";
    [timer updateName:testStr];
    XCTAssertTrue([timer._timerName isEqualToString:testStr]);
    NSString* formattedStr = [NSString stringWithFormat:NSLocalizedString(@"Timer is done: %@", nil), testStr];
    XCTAssertTrue([timer._notification.alertBody isEqualToString:formattedStr]);
}

#pragma mark - Format Interval Tests

- (void)testTimerFormatIntervalSingleDigitSeconds
{
    NSTimeInterval interVal = 5;
    NSString* testString = [timer formatInterval:interVal includeHour:NO];
    XCTAssertTrue( [testString isEqualToString:@"00:05"] );
    
    testString = [timer formatInterval:interVal includeHour:YES];
    XCTAssertTrue( [testString isEqualToString:@"00:00:05"] );
}

- (void)testTimerFormatIntervalDoubleDigitSeconds
{
    NSTimeInterval interVal = 10;
    NSString* testString = [timer formatInterval:interVal includeHour:NO];
    XCTAssertTrue( [testString isEqualToString:@"00:10"] );
    
    testString = [timer formatInterval:interVal includeHour:YES];
    XCTAssertTrue( [testString isEqualToString:@"00:00:10"] );
}

- (void)testTimerFormatIntervalSingleDigitMinutes
{
    NSTimeInterval interVal = 90;
    NSString* testString = [timer formatInterval:interVal includeHour:NO];
    XCTAssertTrue( [testString isEqualToString:@"01:30"] );
    
    testString = [timer formatInterval:interVal includeHour:YES];
    XCTAssertTrue( [testString isEqualToString:@"00:01:30"] );
}

- (void)testTimerFormatIntervalDoubleDigitMinutes
{
    NSTimeInterval interVal = 1200;
    NSString* testString = [timer formatInterval:interVal includeHour:NO];
    XCTAssertTrue( [testString isEqualToString:@"20:00"] );
    
    testString = [timer formatInterval:interVal includeHour:YES];
    XCTAssertTrue( [testString isEqualToString:@"00:20:00"] );
}

- (void)testTimerFormatIntervalSingleDigitHours
{
    NSTimeInterval interVal = 18000;
    NSString* testString = [timer formatInterval:interVal includeHour:NO];
    XCTAssertTrue( [testString isEqualToString:@"05:00:00"] );
    
    testString = [timer formatInterval:interVal includeHour:YES];
    XCTAssertTrue( [testString isEqualToString:@"05:00:00"] );
}

- (void)testTimerFormatIntervalDoubleDigitHours
{
     NSTimeInterval interVal = 54000;
    NSString* testString = [timer formatInterval:interVal includeHour:NO];
    XCTAssertTrue( [testString isEqualToString:@"15:00:00"] );
    
    testString = [timer formatInterval:interVal includeHour:YES];
    XCTAssertTrue( [testString isEqualToString:@"15:00:00"] );
}

- (void)testGetDescriptionActive
{
    timer._active = NO;
    NSString* string = [timer getDescription];
    XCTAssertTrue([string rangeOfString:NSLocalizedString(@"Paused", nil)].location != NSNotFound);
    
    timer._active = YES;
    string = [timer getDescription];
    XCTAssertTrue([string rangeOfString:NSLocalizedString(@"Running", nil)].location != NSNotFound);
}

- (void)testGetDescriptionExpired
{
    timer._expired = NO;
    NSString* string = [timer getDescription];
    XCTAssertTrue([string rangeOfString:@"-"].location == NSNotFound);
    
    timer._expired = YES;
    string = [timer getDescription];
    XCTAssertTrue([string rangeOfString:@"-"].location != NSNotFound);
}

@end

#pragma mark - TimerTestsWithIntervalSizzle
@implementation TimerTestsWithIntervalSizzle

- (void)setUp
{
    [super setUp];
    // Swizzle the timeIntervalSinceNow so we can control it.
    orginalIntervalMethod = class_getInstanceMethod( [NSDate class], @selector(timeIntervalSinceNow) );
    altIntervalMethod = class_getInstanceMethod( [NSDate class], @selector(swizzleTimeIntervalSinceNow) );
    method_exchangeImplementations( orginalIntervalMethod, altIntervalMethod );
}

- (void)tearDown
{
    // Restore
    method_exchangeImplementations( altIntervalMethod, orginalIntervalMethod );
    [super tearDown];
}

- (void)testTimeLeftForTimer
{
    staticInterVal = 20;
    XCTAssertEqual( staticInterVal, [timer timeLeftForTimer], @"" );
}

@end
