//
//  NSDate-Extensions.h
//  SuperKitchenTimer
//
//  Created by Holst, Phil on 8/21/14.
//  Copyright (c) 2014 HolstSoft. All rights reserved.
//

#ifndef SuperKitchenTimer_NSDate_Extensions_h
#define SuperKitchenTimer_NSDate_Extensions_h

static NSTimeInterval staticInterVal;

@interface NSDate (TestingExtensions)
{
	
}

- (NSTimeInterval)swizzleTimeIntervalSinceNow;

@end

@implementation NSDate (TestingExtensions)

- (NSTimeInterval)swizzleTimeIntervalSinceNow
{
	return staticInterVal;
}

@end

#endif
